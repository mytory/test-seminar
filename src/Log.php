<?php
namespace Mytory\Seminar;

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

class Log {
    public static function debug($string) {
        $log = self::getLogger();
        $log->debug($string);
    }

    public static function getLogger(): Logger
    {
        $log = new Logger('seminar');
        $log->pushHandler(new StreamHandler(realpath(__DIR__) . '/../logs/seminar.log', Logger::DEBUG));
        return $log;
    }
}