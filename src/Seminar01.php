<?php

namespace Mytory\Seminar;

use Carbon\Carbon;

class Seminar01
{
    /**
     * @param $date_of_birth  : 생년월일 Y-m-d
     * @param $today  : Y-m-d
     *
     * @return int
     */
    public function getStandardizedAge($date_of_birth, $today = null): int
    {
        $d1 = Carbon::createFromFormat('Y-m-d', $date_of_birth);
        $d2 = Carbon::createFromFormat('Y-m-d', ($today ?? date('Y-m-d')));

        Log::debug($d1);
        Log::debug($d2);

        $years = $d2->year - $d1->year;
        $months = $d2->month - $d1->month;
        $days = $d2->day - $d1->day;

        if ($months < 0 || ($months == 0 && $days < 0)) {
            $years--;
        }
        return $years;
    }

    /**
     * 우리는 알림을 오전 8시부터 밤 10시에 보냅니다.
     * 따라서 새 알림을 생성할 때 지금이 오전 8시부터 밤 10시엔 경우에는 지금부터 5분 후에 보냅니다.
     * 예컨대 지금이 오후 5시 55분이면, 알림은 6시에 예약됩니다.
     * 그런데 지금이 새벽 1시라면 어떻게 해야 할까요? 네. 오전 8시로 예약이 걸려야 합니다.
     * 9시 55분 1초에 발행한 기사는 어떻게 해야 할까요? 자동 예약은 오전 8시로 걸려야 합니다. 10시가 넘었지만 보낼지는 사람이 판단하게 합니다.
     * 7시 56분에 발행한 기사는 어떻게 해야 할까요? 8시 1분에 보냅니다.
     * 이 메서드는 알림 예정 시각을 구하는 메서드입니다.
     *
     * @param $current_time
     *
     * @return string
     */
    public function getNotificationReservationTime($current_time): string
    {
        if ( ! $current_time) {
            $datetime = Carbon::now();
        } else {
            $datetime = Carbon::parse($current_time);
        }

        $sent_at = $datetime->addMinutes(5);
        $standard1 = Carbon::parse($sent_at->format('Y-m-d 22:00:00'));
        $standard2 = Carbon::parse($sent_at->format('Y-m-d 08:00:00'));

        if ($sent_at->greaterThan($standard1)) {
            $sent_at = $datetime->addDay()->setTime(8, 0);
        }

        if ($sent_at->lessThan($standard2)) {
            $sent_at = $datetime->setTime(8, 0);
        }

        return $sent_at->format('Y-m-d H:i:s');
    }
}


